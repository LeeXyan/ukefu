package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.SystemUpdatecon;

public abstract interface SystemUpdateconRepository  extends JpaRepository<SystemUpdatecon,String>{
	
	public abstract SystemUpdatecon findByOrgi(String orgi);
}
